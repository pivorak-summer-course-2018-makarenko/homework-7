class ProfileController < ApplicationController
  before_action :set_account, only: [:deposit, :do_deposit, :withdraw, :do_withdraw]
  before_action :authenticate_user!

  def dashboard
    redirect_to root_path, notice: "You can't view accounts unless you signed in!" unless user_signed_in?
    @user = current_user
    @user_accounts = Account.where(user_id: @user.id)
    @accounts = Account.all
  end

  def info
    @user_accounts = Account.where(user_id: current_user.id)
    @user_accounts.each do |account|
      @user_transactions = Transaction.where(account_id: account.id)
    end
  end

  def withdraw
    @user = current_user
    @accounts = Account.where(user_id: @user.id)
    @prospects = Account.where(withdraw: true)
  end

  def deposit
    @user = current_user
    if @user.id != @account.user.id
      redirect_to root_path, notice: "You can't view another users accounts!"
    end
  end

  def do_deposit
    amount = params[:deposit][:amount].to_i
    check_deposit(amount)

  end

  def do_withdraw
    amount = params[:withdraw][:amount].to_i
    check_withdraw(amount)
  end

  def check_deposit(amount)
    if amount < 0
      redirect_to root_path, notice: "You can't enter negative number! "
    else
      DepositService.new(@account, amount).call

      redirect_to root_path, notice: "You have successfully deposited!"
    end
  end

  def check_withdraw(amount)
    if amount < 0
      redirect_to root_path, notice: "You can't enter negative number! "
    elsif @account.amount - amount < 0
      redirect_to root_path, notice: "You can't withdraw More Than #{@account.amount.to_i} #{@account.currency} From This Account!"
    else
      WithdrawService.new(@account, amount).call

      redirect_to root_path, notice: "You have successfully did withdraw!"
    end
  end
  private

    def set_account
      @account = Account.find(params[:account_id])
    end
end
