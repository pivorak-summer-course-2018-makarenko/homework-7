# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    sequence(:name) { |i| "user#{i}" }
    password 'password'
    email 'localhost123@gmail.com'
    token { SecureRandom.uuid }
  end
end
