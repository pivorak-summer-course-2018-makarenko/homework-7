# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    name 'localhost'
    email 'localhost@gmail.com'
    password '12334567'
  end
end
