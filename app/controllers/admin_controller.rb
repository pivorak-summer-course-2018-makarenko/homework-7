class AdminController < ApplicationController
  before_action :validate_admin

  def notifications
    @user_transactions = Transaction.all
    @admin_transactions = []
    @user_transactions.each do |transaction|
      if transaction.amount.to_i > 5000
        @admin_transactions <<  transaction
      end
    end
  end

  private

  def validate_admin
    unless current_user.try(:admin?)
      redirect_to root_path, notice: "You don't have admin permissions!"
    end
  end
end
