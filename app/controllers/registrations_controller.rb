# frozen_string_literal: true

# TOP CLASS
class RegistrationsController < Devise::RegistrationsController
  def new
    super
  end

  def create
    super
    Account.create(user_id: current_user.id, amount: 0, currency: 'USD')
    Account.create(user_id: current_user.id, amount: 0, currency: 'UAH')
    UserMailer.with(user: @user).welcome_email.deliver_now
  end

  def update
    super
  end

  private

  def sign_up_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end

  def account_update_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation, :current_password)
  end
end
