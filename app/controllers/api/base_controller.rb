module Api
  class BaseController < ActionController::API
    include ActionController::HttpAuthentication::Token::ControllerMethods
    rescue_from ActiveRecord::RecordNotFound, with: :not_found
    rescue_from ActionController::ParameterMissing,with: :bad_request
    before_action :authenticate

    private

    def authenticate
      authenticate_token || render_unathorized
    end

    def authenticate_token
      authenticate_with_http_token do |token,options|
        User.find_by(token: token)
      end
    end

    def render_unathorized
      render json: {
        error_message: 'Bad credentials'
      }, status: :unathorized
    end

    def not_found
      render json: {
        error_message: "Not found"
      }, status: :not_found
    end

    def bad_request
      render json: {
        error_message: "Something wrong"
      },status: :bad_request
    end

    def not_registered
      render json: {
        error_message: "not registered"
      },status: 401
    end
  end
end
