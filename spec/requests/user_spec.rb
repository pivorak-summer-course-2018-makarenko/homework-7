# frozen_string_literal: true

require 'rails_helper'

describe 'api/users', type: :request do
  let(:user) { create(:user) }

  describe 'GET' do
    it 'renders correct response' do
      get '/api/users', headers: { 'Authorization': "Bearer
        #{user.token}" }
      expect response.content_type.to eq('application/json')
      json_body = JSON.parse(response.body)
      expect(json_body.count).to eq(1)
      expect(json_body[0]['name']).to eq(user.name)
      expect(json_body[0]['id']).to eq(user.id)
    end
  end
end
