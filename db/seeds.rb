# frozen_string_literal: true

# This file should contain all the record creation needed to s
# The data can then be loaded with the rails db:seed comma
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
