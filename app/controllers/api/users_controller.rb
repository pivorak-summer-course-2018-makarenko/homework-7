module Api
  class UsersController < BaseController

    def index
      render json: users, each_serializer: UserSerializer
    end

    def show
      render json: user, each_serializer: UserSerializer
    end

    def create
      user = User.new(user_params)
        if user.save
          render json: user, each_serializer: UserSerializer, status: :created
        else
          render json: user.errors, status: :unprocessable_entity
        end
    end

    def update
      if user.update(user_params)
        render json: user, each_serializer: UserSerializer
      else
        render json: user.errors, status: :unprocessable_entity
      end
    end

    def destroy
      user.destroy

      render json: { message: "record deleted",status: :ok}
    end

    def accounts
      render json: user_accounts
    end
    private

    def user
      @user ||= User.find(params[:id])
    end

    def users
      @users ||= User.all
    end

    def user_params
      params.require(:user).permit(:name, :password, :email)
    end

    def user_accounts
      @user_accounts ||= user.accounts
    end

  end
end
