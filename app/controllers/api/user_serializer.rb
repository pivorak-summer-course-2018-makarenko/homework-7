module Api
  class UserSerializer < ActiveModel::Serializer
    attributes :id, :name
  end
end
