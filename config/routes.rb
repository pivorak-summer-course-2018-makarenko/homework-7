Rails.application.routes.draw do
  root to: 'profile#dashboard'
  devise_for :admins
  devise_for :users,:controllers => {:registrations => "registrations"}
  resources :transactions
  resources :accounts
  resources :users

  namespace :api do
    resources :users, only: [:index, :show, :create, :update, :destroy] do
      member do
        get :accounts
      end
    end
  end

  get '/profile/info' => 'profile#info'
  get '/admin/notifications' => 'admin#notifications'
  get '/profile/:account_id/deposit' => 'profile#deposit', as: :deposit
  post '/profile/:account_id/do_deposit' => 'profile#do_deposit', as: :do_deposit

  get '/profile/:account_id/withdraw' => 'profile#withdraw', as: :withdraw
  post '/profile/:account_id/do_withdraw' => 'profile#do_withdraw', as: :do_withdraw


end
